# Introduction & Motivation
- A bit about HPC systems in general
- A bit about number of nodes

# Motivation
- Highlight possible error cases
- Talk about administrative overhead
- Highlight scope / goal: finding irregularities in time series

- Talk about what data is available
- Talk about the problems, issues, how this field is different from regular analysis (privacy thingy)

# Literature overview
- Highlight possible methods
    - Here comes the literature overview
    - Highlight the suffix array / LCP techniques
    - Highlight other things from Genome analysis
- Talk about log analysis using e.g. NLP on normal log data

- Power consumpiton / Temperature data
    - anaomalie detection
    - Use in litature for server/cloud installations
    - Lots todo

# Implementation / Experiments
- Talk about the implementation itself?
- Confirmation using test cases?
- At least runtimes I guess
- Talk about the identified anomalies (TODO)
- Have a look at the reference comparison

- Talk about this idea of correlated anomalies to actually detect stuff

# Results
- Do a comparison with the known not anomnymised log data
    - This raises the question why we even did that in the first part but lets not go there

# Outlook
- How to handle this? Work is not exactly done
- ML and NLP approaches for the other things

Obstacles:
How long actually? - 5 to 8 Pages

What do I actually include?: Describe the akward nature of the data, how this was dealt with : Inlcude the idea of matching patterns or using the timestamps in grouped fashion. Talk about DTW and suffix arrays from area of bioinformatics.
Talk about both ways of finding patterns / subpatterns

When do we have to be finished: 21st!
How much math / runtime analysis?
    - For the report
    - For a presentation
