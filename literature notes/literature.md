https://www.researchgate.net/profile/Cees_Elzinga/publication/228982046_Sequence_analysis_Metric_representations_of_categorical_time_series/links/5464a15e0cf2c0c6aec64294.pdf
SEQUENCE ANALYSIS: METRIC REPRESENTATIONS OFCATEGORICAL TIME SERIES
    - discusses multiple metrics based on string patterns matching like LCP, LCS, etc.
    - Some concepts of shared time

https://www.ebi.ac.uk/Tools/sss/
Sequence Similarity Search
-> COnsider BLASt and FASTA


https://davideliu.com/2019/10/26/log-analysis-for-anomaly-detection/
Overview for anomaly detection - Mostly based on event count vectors for windows

https://cs224d.stanford.edu/reports/YangAgrawal.pdf
Using RNN Language models

A Dynamic Rule Creation Based AnomalyDetection Method for Identifying Security Breachesin Log Records

https://arxiv.org/pdf/1902.08447.pdf
    - AE implementation for anomaly detection in HPC / Data center

https://arxiv.org/pdf/1811.04481v1.pdf
    - Use of CPU utilisation, model based on entropy and average / standard deviation
    

# Time series analysis
## Possible methods for real valued series:
    Fourier Transform
        - Only works if the data is seasonal / periodic
        - Not applicable
    Serial correlation:
        - Correlation with shifted signal
        - Basically: How much previous events afftect the current
            - I would assume none here..


    Stationarity metric
        - Stationary mean / vcariance after e.g removing seasonal effects
    
    E.g. autocorrelation metrics on PCAed data

    InterQUartile Range
        - Points outside of global IQR 

    Isolation Forest
        ???
        
## Basic metrics worth considering
    - Boxplots, Histogram
    - Classical clustering, maybe on multidimensional (multiple columns) of data


## Anomaly analysis in general
    - Autoencoding data and looking for reconstruction accuracy

    - Outlier detection based on distances
        - NLP Embedding techniques could be very inteseting here
    - Frame as a classification problem
        - Problematic due to large class inbalance and unclear labels
        - Proximity based
    
## Online analysis
https://en.m.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm