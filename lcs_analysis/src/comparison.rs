use std::collections::HashSet;

fn mlerp(series: String, window_length: usize){
    let suffix_array = construction(series, window_length);
    let mut spl = 1;
    let mut is_completed = false;
    while !is_completed {
        let tmp = ();
    }

}

trait Sorted {
    fn find_all(pattern: &str) -> (usize, usize);
}

impl Sorted for Vec<String> {
    fn find_all(pattern: &str) -> (usize, usize) {
        let mut l = 0;
        let mut r = 0;
        while l < r {
            let middle = (l + r)/2;
        }
        ;
    }
}
fn max_len_substrings(x: String, count: usize, min_len: usize, max_len: usize) -> Vec<String> {
    let mut output = vec![];
    let suffix_array = construction(x.clone(), max_len);
    let mut x_calculated = false;
    let alphabet: HashSet<char> = x.chars().into_iter().collect();

    for letter in alphabet {
        let new_x = x.clone() + &letter.to_string();
        let new_count = suffix_array.iter().filter(|&x|x.starts_with(&new_x)).count();
        if new_count == count && x.len() < max_len {
            output.append(max_len_substrings(new_x, new_count, min_len, max_len));
        }

        if count > 1 && x.len() == max_len && !x_calculated {
            let positions = suffix_array.iter().enumerate().filter(|(i, suffix)|)
            x_calculated = true;
        }
    }
}

fn construction(x: String, longest_expected: usize) -> Vec<String> {
    let mut suffix_array = vec![];
    let mut lerp = longest_expected;
    for i in 0..x.len() {
        if lerp + i > x.len(){
            lerp = x.len() - i;
        }
        let substring = &x[i..lerp];

        // Just why?
        suffix_array.push(substring.to_string());
    }
    suffix_array.sort();
    return suffix_array
}