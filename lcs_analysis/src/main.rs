extern crate csv;
use plotters::prelude::*;
use rayon::prelude::*;
use std::env;
use serde::{Deserialize, Serialize};
use std::{cmp::min, collections::HashMap, collections::{hash_map::Entry, HashSet}, fs::File};
use std::{hash::Hash, io::prelude::*};
use viz::draw_image;
mod viz;
//mod comparison;

#[derive(Serialize, Deserialize)]
struct Pattern<T> {
    start_pos: Vec<usize>,
    length: usize,
    pattern: Vec<T>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {

    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);
    let input_f = &args[1];
    let output_prefix = &args[2];

    let mut csv_rdr = csv::Reader::from_path(input_f).unwrap();
    let mess: Vec<String> = csv_rdr
        .records()
        .map(|r| r.unwrap().get(1).unwrap().to_string())
        .collect();

    let ((_, back), tokenized) = tokenize(&mess);

    println!("Size of Alphabet: {}", back.len());

    let sa = SuffixArray::new(&tokenized);
    let mut repeating = sa.repeating_substrings(16, 8);
    let repeating_patterns: Vec<Pattern<usize>> = repeating
        .iter_mut()
        .map(|(p, (len, occ))| Pattern {
            pattern: p.to_vec(),
            length: *len,
            start_pos: occ.drain(..).collect(),
        })
        .collect();

    let back_transformed = back_transform(repeating_patterns, &back);

    let json = serde_json::to_string(&back_transformed).expect("Error serialising!");
    File::create(format!("{}.json", output_prefix))?.write_all(&json.as_bytes())?;
    println!("finished writing out {} exlusive patterns", back_transformed.len());

    let mut input_f = File::create("input.txt")?;
    input_f.write_all(mess.join(", ").as_bytes())?;

    let mut all_repeating = sa.all_repeating(16, 8);
    let all_repeating_patterns: Vec<Pattern<usize>> = all_repeating
        .iter_mut()
        .map(|(p, (len, occ))| Pattern {
            pattern: p.to_vec(),
            length: *len,
            start_pos: occ.drain(..).collect(),
        })
        .collect();
    let all_repeating_patterns_str = back_transform(all_repeating_patterns, &back);

    let json_all = serde_json::to_string(&all_repeating_patterns_str).expect("Error serialising!");
    File::create(format!("{}_all.json", output_prefix))?.write_all(&json_all.as_bytes())?;
    println!("finished writing out {} inclusive patterns", all_repeating_patterns_str.len());

    Ok(())
}

struct SuffixArray<T> {
    text: Vec<T>,
    n: usize,
    suffixes: Vec<usize>,
    lcp: Vec<usize>,
}

fn tokenize(
    tokens: &Vec<String>,
) -> (
    (HashMap<&String, usize>, HashMap<usize, &String>),
    Vec<usize>,
) {
    let set: HashSet<&String> = tokens.into_iter().collect();

    let map: HashMap<&String, usize> = set.iter().enumerate().map(|(k, v)| (*v, k)).collect();
    let map_reversed: HashMap<usize, &String> =
        set.iter().enumerate().map(|(k, v)| (k, *v)).collect();

    let tokenized = tokens.iter().map(|k| map[k]).collect();
    return ((map, map_reversed), tokenized);
}

fn back_transform(
    tokenized: Vec<Pattern<usize>>,
    map: &HashMap<usize, &String>,
) -> Vec<Pattern<String>> {
    return tokenized
        .iter()
        .map(|p| {
            let str_sequence: Vec<String> = p
                .pattern
                .iter()
                .map(|token| { map[token] }.clone())
                .collect();
            Pattern {
                length: p.length,
                start_pos: p.start_pos.clone(),
                pattern: str_sequence,
            }
        })
        .collect();
}

impl<T: std::cmp::Ord + Sized + Send + Sync + Clone + std::hash::Hash> SuffixArray<T> {
    fn new(text: &Vec<T>) -> Self {
        let owned = text.clone();
        let n = owned.len();
        let mut suffixes: Vec<usize> = (0..n).collect();

        //Optimisation: There are some O(n) algorithms exploiting the datas structure of sorting
        suffixes.par_sort_by_key(|i| &owned[*i..]);

        let mut sa = SuffixArray {
            text: owned,
            n,
            suffixes,
            lcp: Vec::new(),
        };

        sa.lcp = (1..n).into_par_iter().map(|i| sa.lcp(i, i - 1)).collect();
        sa
    }

    fn lcp(&self, i: usize, j: usize) -> usize {
        self.text[self.suffixes[i]..]
            .iter()
            .zip(self.text[self.suffixes[j]..].iter())
            .take_while(|(a, b)| *a == *b)
            .count()
    }


    fn all_repeating(&self, max_len: usize, min_len: usize) -> HashMap<&[T], (usize, Vec<usize>)> {
        //TODO: This doesnt fit into memory for larger maximum sizes: Quadratic size requirement for results
        //Streaming results to disk immediatly might be an option
        let repetitions: Vec<(Vec<usize>, usize, &[T])> = (min_len..max_len).into_par_iter().map(|pattern_len|{
            (1..self.n - 1)
            .into_par_iter()
            .map(|i| {
                let length = self.lcp[i];
                if length >= min_len {
                    let cutoff_len = min(pattern_len, length);
                    let indices = vec![self.suffixes[i], self.suffixes[i - 1]];
                    let text = &self.text[self.suffixes[i]..self.suffixes[i] + cutoff_len];
                    Some((indices, cutoff_len, text))
                } else {
                    None
                }
            })
            .filter(Option::is_some)
            .map(Option::unwrap)
            .collect::<Vec<(Vec<usize>, usize, &[T])>>()
        })
        .flatten()
        .collect();

        let mut grouped: HashMap<&[T], (usize, HashSet<usize>)> = HashMap::new();
        for (mut occurances, len, pattern) in repetitions {
            grouped
                .entry(pattern)
                .and_modify(|(_, v)| {
                    occurances.iter().for_each(|occ| {
                        v.insert(*occ);
                    })
                })
                .or_insert((len, occurances.drain(..).collect()));
        }
        return grouped
            .drain()
            .map(|(v, (len, occ))| {
                let mut sorted_occ: Vec<usize> = occ.clone().drain().collect();
                sorted_occ.sort();
                (v, (len, sorted_occ))
            })
            .collect();
    }

    fn repeating_substrings(
        &self,
        max_len: usize,
        min_len: usize,
    ) -> HashMap<&[T], (usize, Vec<usize>)> {
        
        //find indices in lcp array until which values are increasing, cut off at max length
        let (_, to_search) = self.lcp.iter().enumerate().fold(
            (0usize, vec![0]),
            |(last, mut longest_indices), (idx, &prefix_len)| {

                if prefix_len > max_len {
                    // start new pattern
                    longest_indices.push(idx);
                    
                } else {
                    match prefix_len.cmp(&last) {
                        std::cmp::Ordering::Less => {
                            // is this part of a larger pattern or just one longer?
                            if (prefix_len + 1) != last {
                                //new pattern
                                longest_indices.push(idx)
                            }
                        }
                        std::cmp::Ordering::Equal => {
                            // same pattern again
                            longest_indices.push(idx)
                        }
                        std::cmp::Ordering::Greater => {
                            //subpattern of larger one later on
                            *longest_indices.last_mut().unwrap() = idx;
                        }
                    };    
                }
                (prefix_len, longest_indices)

            },
        );

        //Only construct the multiples for these
        let repetitions: Vec<(Vec<usize>, usize, &[T])> = to_search
            .into_par_iter()
            .filter(|x| *x != 0)
            .map(|i| {
                let length = self.lcp[i];
                if length >= min_len {
                    let cuttoff_len = min(max_len, length);
                    let indices = vec![self.suffixes[i], self.suffixes[i - 1]];
                    let text = &self.text[self.suffixes[i]..self.suffixes[i] + cuttoff_len];
                    Some((indices, cuttoff_len, text))
                } else {
                    None
                }
            })
            .filter(Option::is_some)
            .map(Option::unwrap)
            .collect();

        let mut grouped: HashMap<&[T], (usize, HashSet<usize>)> = HashMap::new();
        for (mut occurances, len, pattern) in repetitions {
            grouped
                .entry(pattern)
                .and_modify(|(_, v)| {
                    occurances.iter().for_each(|occ| {
                        v.insert(*occ);
                    })
                })
                .or_insert((len, occurances.drain(..).collect()));
        }
        return grouped
            .drain()
            .map(|(v, (len, occ))| {
                let mut sorted_occ: Vec<usize> = occ.clone().drain().collect();
                sorted_occ.sort();
                (v, (len, sorted_occ))
            })
            .collect();
    }
}

struct LongestRepeatingSubsequence<T: Hash + std::cmp::Eq> {
    text: Vec<T>,
    cache: HashMap<(usize, usize), usize>,
}

impl<'a, T: std::cmp::Eq + Clone + Hash> LongestRepeatingSubsequence<T> {
    fn new(text: &Vec<T>) -> Self {
        LongestRepeatingSubsequence {
            text: text.clone(),
            cache: HashMap::new(),
        }
    }
    fn lrs(&mut self, m: usize, n: usize) -> Vec<T> {
        if m == 0 || n == 0 {
            return vec![];
        }
        if (self.text[m - 1] == self.text[n - 1]) && m != n {
            let mut tmp = self.lrs(m - 1, n - 1);
            tmp.push(self.text[m - 1].clone());
            return tmp;
        } else {
            if self.lrs_length(m - 1, n) > self.lrs_length(m, n - 1) {
                return self.lrs(m - 1, n);
            } else {
                return self.lrs(m, n - 1);
            }
        }
    }

    fn lrs_length(&mut self, m: usize, n: usize) -> usize {
        if let Some(res) = self.cache.get(&(m, n)) {
            return *res;
        } else {
            if m == 0 || n == 0 {
                self.cache.insert((m, n), 0);
                return 0;
            }
            if (self.text[m - 1] == self.text[n - 1]) && m != n {
                let r = self.lrs_length(m - 1, n - 1) + 1;
                self.cache.insert((m, n), r);
                return r;
            }
            let r = std::cmp::max(self.lrs_length(m, n - 1), self.lrs_length(m - 1, n));
            self.cache.insert((m, n), r);
            return r;
        }
    }
}
