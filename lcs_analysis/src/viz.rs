use plotters::{coord::Shift, prelude::*};

pub fn draw_image(
    area: &DrawingArea<BitMapBackend, Shift>,
    row_len: usize,
    sequence: Vec<String>,
) -> Result<(), Box<dyn std::error::Error>> {
    let elements = area.split_evenly((row_len, sequence.len() / row_len));
    let colour_thingies: Vec<u32> = sequence
        .join("")
        .chars()
        .map(|c| c.to_digit(16).unwrap())
        .collect();
    for (idx, subarea) in (0..).zip(elements.iter()) {
        subarea.fill(&Palette99::pick(colour_thingies[idx] as usize))?;
    }
    Ok(())
}
pub fn draw_duplicates(
    area: &DrawingArea<BitMapBackend, Shift>,
    row_len: usize,
    series: Vec<usize>,
    duplicates: Vec<(usize, usize, usize)>,
) -> Result<(), Box<dyn std::error::Error>> {
    let elements = area.split_evenly(((series.len() as f64 / row_len as f64).ceil() as usize, row_len));

    for (ident, idx, len) in duplicates.iter().filter(|(_, _, len)|{ len > &0}) {
        for offset in 0..=*len {
            elements[*idx + offset].fill(&Palette99::pick(*ident))?;
        }
    }
    Ok(())
}
