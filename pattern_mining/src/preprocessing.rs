use std::collections::{HashMap, HashSet};

fn tokenize(
    tokens: &Vec<String>,
) -> (
    (HashMap<&String, usize>, HashMap<usize, &String>),
    Vec<usize>,
) {
    let set: HashSet<&String> = tokens.into_iter().collect();

    let map: HashMap<&String, usize> = set.iter().enumerate().map(|(k, v)| (*v, k)).collect();
    let map_reversed: HashMap<usize, &String> =
        set.iter().enumerate().map(|(k, v)| (k, *v)).collect();

    let tokenized = tokens.iter().map(|k| map[k]).collect();
    return ((map, map_reversed), tokenized);
}