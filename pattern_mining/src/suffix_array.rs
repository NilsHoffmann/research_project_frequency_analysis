use rayon::prelude::*;

struct SuffixArray<T> {
    text: Vec<T>,
    n: usize,
    suffixes: Vec<usize>,
    lcp: Vec<usize>,
}

impl<T: std::cmp::Ord + Sized + Send + Sync + Clone> SuffixArray<T> {
    fn new(text: &Vec<T>) -> Self {
        let owned = text.clone();
        let n = owned.len();
        let mut suffixes: Vec<usize> = (0..n).collect();

        //Optimisation: There are some O(n) algorithms exploiting the datas structure of sorting
        suffixes.par_sort_by_key(|i| &owned[*i..]);

        let mut sa = SuffixArray {
            text: owned,
            n,
            suffixes,
            lcp: Vec::new(),
        };

        sa.lcp = (1..n).into_par_iter().map(|i| sa.lcp(i, i - 1)).collect();
        sa
    }

    fn lcp(&self, i: usize, j: usize) -> usize {
        self.text[self.suffixes[i]..]
            .iter()
            .zip(self.text[self.suffixes[j]..].iter())
            .take_while(|(a, b)| *a == *b)
            .count()
    }

    fn repeating_substrings(&self, max_len: usize, min_len: usize) -> Vec<(usize, usize, &[T])> {
        let t: Vec<_> = (1..self.n-1)
            .into_par_iter()
            .map(|i| {
                let length = self.lcp[i];
                if length >= min_len && max_len > length {
                    Some((
                        i,
                        length,
                        &self.text[self.suffixes[i]..self.suffixes[i] + length],
                    ))
                } else {
                    None
                }
            })
            .filter(Option::is_some)
            .map(Option::unwrap)
            .collect();
        t
    }
}