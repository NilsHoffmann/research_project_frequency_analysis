use pyo3::prelude::*;
 

mod preprocessing;
mod suffix_array;


#[pyfunction]
fn tokenize(tokens: Vec<String>) -> Vec<usize> {
    let ((map, reverse), tokenized) =  preprocessing::tokenize(&tokens); 
    return tokenized;
}