import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from sklearn.cluster import KMeans
import plotly.graph_objects as go
import plotly.express as plx
import torch
from functools import partial
from torch import nn
import torch.nn.functional as F
from tqdm import tqdm

import tensorflow as tf
from tensorflow.keras import Model, Sequential, layers, losses, optimizers
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import plotly.express as px
import numpy as np
import json
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from scipy.optimize import curve_fit
from scipy.special import factorial
from scipy.stats import poisson, norm, uniform
import statsmodels.api as sm
import os


def load_data(path):
    df = pd.read_csv(path)
    df = df.drop(df.columns[0], 1)
    df.index = pd.to_datetime(df["ref_ts"])
    df = df.drop("ref_ts", 1)
    return df


def load_patterns(json_filepath, min_occurances = 1000):
    patterns = pd.read_json(json_filepath)
    patterns["occurances"] = patterns.start_pos.map(len)
    patterns["first"] = patterns.start_pos.map(lambda p: p[0])
    patterns["last"] = patterns.start_pos.map(lambda p: p[-1])
    patterns = patterns.query(f"occurances > {min_occurances}")
    return patterns

def load_events(csv_path):
    events = pd.read_csv(csv_path)
    events.columns = ["timestamp", "event"]
    events = events.sort_values("timestamp")
    events.timestamp = events.timestamp
    #start = events.timestamp.min()
    #zeroed = events.timestamp.copy() - start
    #norm = zeroed / zeroed.max()
    events["readable_ts"] = pd.to_datetime(events.timestamp, unit="s")
    #events["norm_ts"] = norm
    return events


def resample(df):
    new_index = pd.date_range(df.index.min(), df.index.max(), 2_000_000)
    new_df = pd.DataFrame(index=new_index)
    for column in df.columns:
        new_c = np.interp(new_index, df.index, df[column])
        new_df[column] = new_c
    return new_df

def train_ae(df, window_length=4, windows=20_000, epochs=50, batch_size=2048):

    take_idxs = np.random.choice(np.arange(0, len(df)), windows, replace = False)
    take_idxs = take_idxs[take_idxs < len(df) - window_length]
    data = df.values[take_idxs.repeat(window_length).reshape(-1, window_length) + np.arange(window_length)]
    data -= np.min(data, axis=(0,1))
    data /= data.max(axis=(0,1))
    latent_dim = 64
    enc = Sequential([
        layers.Flatten(),
        layers.Dense(128, input_dim=window_length*5, activation="relu"),
        layers.Dense(128, activation="relu"),
        layers.Dense(128, activation="relu"),

        layers.Dense(latent_dim, activation="relu")
    ])

    dec = Sequential([
        layers.Dense(128, input_dim=latent_dim, activation="relu"),
        layers.Dense(128, activation="relu"),
        layers.Dense(128, activation="relu"),

        layers.Dense(window_length*5, activation="sigmoid"),
        layers.Reshape((window_length,5))
    ])

    inputs = layers.Input((window_length, 5))
    ae = Model(inputs, dec(enc(inputs)))
    ae.compile(optimizer="adam", loss="mse")
    ae.fit(data, data, batch_size=batch_size, epochs=epochs, verbose=1, shuffle=False)
    data_ref = df.values[:df.values.shape[0] // (window_length * 5) * window_length * 5, :].reshape(-1, window_length, 5)
    dist = tf.keras.losses.mse(ae.predict(data_ref), data_ref)
    return dist

def get_outages_go(outages_df, y_shift=0):
    return go.Scatter(x=outages_df.star_h, y=np.zeros(len(outages_df)) + y_shift, 
                                        mode='markers', 
                                        marker_symbol = "x",
                                        name = "known outages",
                                        marker={"color": "blue", "line": {"width": 0}})


def add_pattern_ts(pattern_df, event_df):
    event_ts = []
    for i, row in enumerate(pattern_df.iterrows()):
        pattern = row[1]
        idx = np.array(pattern["start_pos"])
        length = pattern["length"]

        event_xs = event_df.timestamp.iloc[idx].values
        event_ts.append(event_xs)
        
    pattern_df["timestamps"] = event_ts
    return pattern_df

def add_deltas(pattern_df):
    deltas = [(ts[1:] - ts[:-1]) for ts in pattern_df.timestamps]
    pattern_df["deltas"] = deltas
    return pattern_df


def filter_zero_deltas(pattern_df):
    filter_mask = pattern_df.apply(lambda r: (r["deltas"] == 0).sum() <= 0.1 * r["deltas"].size, axis=1)
    return pattern_df[filter_mask]


def find_delta_anom(pattern_df):
    def fit_exp(deltas):
        removed = deltas.copy()
        thresh = np.inf
        for _ in range(5):
            removed = removed[removed < thresh]
            c = 1 / removed.mean()
            p = 0.01 / 100 #0.1 % chance of outlier, adjust accordingly
            thresh = - np.log(p) / c
        return thresh
    
    pattern_df["thresholds"] = pattern_df.deltas.map(fit_exp)
    pattern_df["anom_ts"] = pattern_df.apply(lambda r: r["timestamps"][:-1][
        (r["deltas"] > r["thresholds"]) 
        #& (r["deltas"] < 10 * r["thresholds"])dh
        
    ], axis=1)
    pattern_df["cnt_anom"] = pattern_df.anom_ts.map(lambda v: len(v))
    return pattern_df

def get_anomaly_go(y_shift, xs, name):
    return go.Scatter(x = pd.to_datetime(xs, unit="s"),
                       y = y_shift,
                       mode = "markers",
                       name = name
                      )

def plot_predictions(anomalies, outages_df):
    fig = go.FigureWidget([get_outages_go(outages_df)])
    for i, a in enumerate(anomalies):
        fig.add_trace(
            get_anomaly_go(np.ones_like(a) + i + 15, a, f"pattern {i}")
        )

    fig.show()


def combine_norm_dist(df, time_res, var=60 * 60 * 3):
    
    return np.sum([
        np.sum(np.array([
            uniform(ts, var).pdf(time_res)
            for ts in anomaly_ts
        ]), axis=0)
        for anomaly_ts in df.anom_ts.values
    ], axis=0)


def print_confidences(y, time_res):
    threshold_factor = 0.2
    time_threshold_factor = (time_res[1] - time_res[0]) * 100

    
    thresh_h = time_res[y > y.max() * threshold_factor]
    confidence_vals = y[y > y.max() * threshold_factor]
    peaks = thresh_h[:-1][(thresh_h[1:] - thresh_h[:-1]) > time_threshold_factor]
    confidence_vals = confidence_vals[1:][(thresh_h[1:] - thresh_h[:-1]) > time_threshold_factor]
    confidences = confidence_vals / confidence_vals.max()
    times = pd.to_datetime(peaks, unit="s")
    frmt = "%Y-%m-%d %H:%M"
    return "\n".join([f"anomaly @ {b.strftime(frmt)}: confidence {a*100 :2.1f}%" for a,b in list(zip(confidences, times))])


def load_ref_outages(path):
    out = pd.read_csv(path)
    out = out[out.node == "taurusi5314"]
    out.star_h = pd.to_datetime(out.star_h)
    return out


def get_variances(df):
    roll_var = df.resample("4h").std()
    return roll_var

def get_window(num_df, events_df, start, window_size):
    num_mask = (num_df.index >= start) & (num_df.index < start + window_size)
    event_mask = (events_df.readable_ts >= start) & (events_df.readable_ts < start + window_size) 
    return num_df[num_mask].copy(), events_df[event_mask].copy()


def get_patterns(event_df, min_occurances=1000, include_subpatterns=True):
    event_df.to_csv("../data/tmp.csv", index=False, columns=["readable_ts", "event"])
    os.system("../mine_patterns ../data/tmp.csv ../data/tmp_out")
    return load_patterns("../data/tmp_out_all.json" if include_subpatterns else "../data/tmp_out.json", min_occurances)

def get_outages_window(outage_df, start, window_size):
    mask = (outage_df.star_h >= start) & (outage_df.star_h < start + window_size)
    return outage_df[mask].copy()